<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\SendReminderEmail;
use App\User;
use Illuminate\Http\Request;

class UsersResource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return $users->toJson(JSON_UNESCAPED_UNICODE);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if($request->has('email')) {
            $email = $request->input('email');

            $user = User::where('email', '=', $email)->first();
            if(!is_null($user)){
                return json_encode(['error' => 'Пользователем с таким Email уже существует'], JSON_UNESCAPED_UNICODE);
            } else {
                $user = new User;
                $user->email = $email;
                $user->name = $request->input('name');
                $user->surname = $request->input('surname');
                $user->measure_id = $request->input('measure_id');
                $user->save();
                dispatch((new SendReminderEmail($email))->onQueue('emails'));
                return json_encode(['response' => "Пользователь с email = $email успешно добавлен"], JSON_UNESCAPED_UNICODE);
            }
        }


        return json_encode(['error' => 'email Не прередан'], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        $user = User::where('id', '=', $id)->get();
        return $user->toJson(JSON_UNESCAPED_UNICODE);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->has('email')) {
            $email = $request->input('email');

            $user = User::find($id);
            if(is_null($user)){
                return json_encode(['error' => 'Пользователя с таким id не существует'], JSON_UNESCAPED_UNICODE);
            } else {
                $user->name = $request->input('name');
                $user->surname = $request->input('surname');
                $user->measure_id = $request->input('measure_id');
                $user->save();
                return json_encode(['response' => "Обновление пользователя с id = $id успешно выполнено"], JSON_UNESCAPED_UNICODE);
            }
        }

        return json_encode(['error' => 'email Не прередан'], JSON_UNESCAPED_UNICODE);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $user = User::destroy($id);

        return json_encode(['response' => 'Пользователь с id = '.$id. ' удалён из базы'], JSON_UNESCAPED_UNICODE);
    }
}
