<?php

namespace App\Http\Middleware;
use Closure;

class CheckAuth
{

    const API_KEY_HEADER = 'x-api-key';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header(self::API_KEY_HEADER);

        if ($token === null) {
            die( json_encode(['error' => 'Unauthorized'], JSON_UNESCAPED_UNICODE));
            //return $this->sendError('Unauthorized.', 401);
        }

        if ($token !== config('services.api.token')) {
            die( json_encode(['error' => 'Unauthorized'], JSON_UNESCAPED_UNICODE));
        }

        return $next($request);
    }
}
